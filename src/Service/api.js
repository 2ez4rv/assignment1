import axios from 'axios';

const baseUrl ='https://glacial-waters-01424.herokuapp.com/api/';

export const getUsers = async () => {
    return await axios.get('https://glacial-waters-01424.herokuapp.com/api/emp');
}

export const newUser = async (user,url) => {
     await axios.post(baseUrl+url,user);
}