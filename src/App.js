import './App.css';
import LoginScreen from './LoginScreen';
import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import HomeScreen from './HomeScreen';

function App() {

  const user = !null;

  return (
    <div className="app">

    <Router>
      {!user ? (
        <LoginScreen />
      ): (
          <Switch>
            <Route exact path="/homescreen">
              <HomeScreen />
            </Route>
          </Switch>
      )}
        
    </Router>

    </div>
  );
}

export default App;
