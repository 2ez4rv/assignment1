import Header from "./Header";
import "./LoginScreen.css"
import Modal from '@material-ui/core/Modal';
import {makeStyles} from '@material-ui/core/styles';
import { useState } from "react";
import { Button , Input} from '@material-ui/core';
import { newUser } from "./Service/api";
import axios from 'axios'
import {useHistory} from 'react-router-dom'


function getModalStyle() {  //MaterialUI styling for user authentication modal.
    const top = 50;
    const left = 50;
  
    return {
      top: `${top}%`,
      left: `${left}%`,
      transform: `translate(-${top}%, -${left}%)`,
    };
  }
  
  const useStyles = makeStyles((theme) => ({  ////MaterialUI styling for user authentication modal.
    paper: {
      position: 'absolute',
      width: 400,
      backgroundColor: theme.palette.background.paper,
      border: '2px solid #000',
      boxShadow: theme.shadows[5],
      padding: theme.spacing(2, 4, 3),
    },
  }));


function LoginScreen() {


    let history = useHistory(); 
  const classes = useStyles();
  const[modalStyle]= useState(getModalStyle);
  
  const [open, setOpen] = useState(false);
  const  [name,setName] = useState('');
  const  [mobile,setMobile] = useState('');
  const  [email,setEmail] = useState('');
  const [city, setCity] = useState('');
  const  [password,setPassword] = useState('');

  const [user, setUser] = useState({
    fname:'',
    mobile:'',
    email:'',
    city:'',
    password:''
  });

    const handleRegister = async (e) => {
        e.preventDefault();
            const newUser = {
                fname: name,
                mobile: mobile,
                email: email,
                city: city,
                password: password
              }
            setUser({...user,newUser});
            await axios.post("https://glacial-waters-01424.herokuapp.com/api/newUser",user);
            setOpen(false);
            setName('');
            setMobile('');
            setEmail('');
            setPassword('');
            setCity('');
                
    }
    
    const handleLogin = async (e) => {
        e.preventDefault();
        const loginUser = {
            email: email,
            password: password
          }
          setUser({...user,loginUser})
          
        await axios.post("https://glacial-waters-01424.herokuapp.com/api/login",user);
        
        setEmail('');
        setPassword('');
        history.push('/homescreen');
    }

    localStorage.setItem('assessToekn',"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6IiIsInBhc3N3b3JkIjoiIiwiaWF0IjoxNjI3NjMwNzAxLCJleHAiOjE2Mjc3MTcxMDF9.TNUy4f65D1x9GOqDj1xZE62lUHv68dTtgaWUV17sef0");

    return (
        
        <div>
        <Header />    
        <div className="feed">
            
        <div className="LoginScreen">

<Modal
  open={open}
  onClose={() => setOpen(false)} 
>
<div style={modalStyle} className={classes.paper}>

<form 
    className="register__form"
    >

<Input
type= "text"
placeholder="Name"
name='fname'
value={name}
onChange={(e) => {setName(e.target.value)}}
/>

<Input
type= "text"
placeholder="Mobile number"
name='mobile'
value={mobile}
onChange={(e) => {setMobile(e.target.value)}}
/>

<Input
type= "text"
placeholder="email"
name='email'
value={email}
onChange={(e) => {setEmail(e.target.value)}}
/>

<Input
type= "text"
placeholder="City"
name='city'
value={city}
onChange={(e) => {setCity(e.target.value)}}
/>

<Input
type= "password"
placeholder="Password"
name='password'
value={password}
onChange={(e) => {setPassword(e.target.value)}}
/>

<Button type="submit" variant="contained" color="primary" onClick={handleRegister}>Register</Button>

</form>
</div>
</Modal>


            <form>
            <h1>Login</h1>
            <input 
                type= "text"
                placeholder="email"
                name='email'
                value={email}
                onChange={(e) => {setEmail(e.target.value)}}
            ></input>

            <input
            type= "password"
            placeholder="Password"
            name='password'
            value={password}
            onChange={(e) => {setPassword(e.target.value)}}
            ></input>

            <button type="submit" onClick={handleLogin}>LOGIN</button>

            <h4> <span className="loginScreen__gray"> Click here to </span>
            <span className="loginScreen__link" onClick={() => {setOpen(true)}}>Register</span>
            </h4>
            </form>
        </div>
        </div>
        </div>
    )
}

export default LoginScreen
