import './Header.css'

function Header() {
    return (
        <div className="header">
            <h1>Softflame test app</h1>
        </div>
    )
}

export default Header
