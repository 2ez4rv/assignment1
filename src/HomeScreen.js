import React , {useState, useEffect} from 'react';
import "./HomeScreen.css";
import {makeStyles} from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import { Button , Input, TableHead,Table,TableRow,TableCell,TableBody } from '@material-ui/core';
import { getUsers } from './Service/api';

function getModalStyle() {  //MaterialUI styling for user authentication modal.
    const top = 50;
    const left = 50;
  
    return {
      top: `${top}%`,
      left: `${left}%`,
      transform: `translate(-${top}%, -${left}%)`,
    };
  }
  
  const useStyles = makeStyles((theme) => ({  ////MaterialUI styling for user authentication modal.
    paper: {
      position: 'absolute',
      width: 400,
      backgroundColor: theme.palette.background.paper,
      border: '2px solid #000',
      boxShadow: theme.shadows[5],
      padding: theme.spacing(2, 4, 3),
    },
  }));
  

function HomeScreen() {

    const[ users , setUsers] = useState([]);
    const[ searchTerm , setSearchTerm]=useState('');

    const getAllUsers = async() => {
        const response= await getUsers();
        console.log(response.data);
        setUsers(response.data);
    }

    useEffect(() => {
        getAllUsers();
    },[])

    const classes = useStyles();
    const[modalStyle]= useState(getModalStyle);
    
    const [open, setOpen] = useState(false);
    const  [name,setName] = useState('');
    const  [email,setEmail] = useState('');
    const  [mobile,setMobile] = useState('');

    
    return (
        
        <div className="homeScreen">


<Modal
  open={open}
  onClose={() => setOpen(false)} 
>
<div style={modalStyle} className={classes.paper}>

<form className="add__record">

<Input
type= "text"
placeholder="Name"
value={name}
onChange={(e) => setName(e.target.value)}
/>

<Input
type= "text"
placeholder="email"
value={email}
onChange={(e) => setEmail(e.target.value)}
/>

<Input
type= "text"
placeholder="Mobile number"
value={mobile}
onChange={(e) => setMobile(e.target.value)}
/>

<Button type="submit">Add to Records</Button>

</form>
</div>
</Modal>


            <div className="header__homeScreen">
                <h1>CRUD</h1>
                <input type="text" placeholder="Search" onChange={event => {setSearchTerm(event.target.value)}} />
                
                {/* {
                    data.map((val,key) => (
                        <div>{val.name}</div>
                    ))
                } */}
                <button onClick={() => {setOpen(true)}}>+</button>
            </div>
            
            <div className="table">
            <Table>
                <TableHead>
                    
                <TableRow className="table__row">
                    <TableCell className="table__column">ID</TableCell>
                    <TableCell className="table__column">Name</TableCell>
                    <TableCell className="table__column">Mobile</TableCell>
                    <TableCell className="table__column">Email</TableCell>
                    <TableCell className="table__column">Action</TableCell>
                </TableRow>

                </TableHead>

                <TableBody>
                {
                    users && users.length > 0 ?
                    (users.filter((val) => {
                        if(searchTerm == "") {
                            return val
                        } else if(val.name.toLowerCase().includes(searchTerm.toLowerCase())) {
                            return val
                        }
                    }).map(user => 
                        (
                        
                        <TableRow className="table__row" key={user._id}>
                            <TableCell className="table__column">{user.emp_id}</TableCell>
                            <TableCell className="table__column">{user.name}</TableCell>
                            <TableCell className="table__column">{user.mobile}</TableCell>
                            <TableCell className="table__column">{user.email}</TableCell>
                            <TableCell>Address</TableCell>
                        </TableRow>
                        
                        )       
                        )
                        )
                        : 'loading'
                }

                </TableBody>
            </Table>
            
            </div>
            

        </div>
    )
}

export default HomeScreen
